from bandit_scan import main as bandit_main
from piprot_scan import main as piprot_main


def main():
    piprot_main()
    bandit_main()


if __name__ == "__main__":
    main()
