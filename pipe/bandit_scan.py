import json
import os
import sys
import tempfile
import subprocess  # nosec

import requests

ENV_VARS = [
    "BITBUCKET_PR_ID",
    "BITBUCKET_REPO_SLUG",
    "BITBUCKET_WORKSPACE",
    "BITBUCKET_CLONE_DIR",
    "BITBUCKET_USER",
    "BITBUCKET_PWD",
    "SCAN_PATH"
]


def __check_env_vars(env_vars: [str]) -> None:
    """
    Check if all environment variables are present
    :return: None
    """
    for var in env_vars:
        try:
            os.environ[var]
        except KeyError as e:
            print(f"Environment variable {var} not set!", file=sys.stderr)
            raise e


def __bandit_report(scan_path: str, prepend_path: str) -> str:
    tmpdir = tempfile.mkdtemp()
    predictable_filename = 'bandit_output.json'

    path = os.path.join(tmpdir, predictable_filename)

    file = open(path, 'w+')

    prepended_scan_path = f"{prepend_path}/{scan_path}"
    args = ['bandit', '-r', '-f', 'json', '-o', path, prepended_scan_path]
    subprocess.Popen(args, shell=False).wait()

    # Get output of bandit, remove last 2 lines (exited + empty line)
    bandit_output = file.read()
    os.remove(path)
    bandit_json = json.loads(bandit_output)

    # remove nesting
    for i in bandit_json["results"]:
        if i["filename"].startswith(prepended_scan_path+'/'):
            i["filename"] = i["filename"][len(prepended_scan_path+'/'):]

    return bandit_json


def __get_request_body(issue: str) -> dict:
    raw_string = issue["issue_text"] + "    \n" + issue["more_info"]

    content = {
        "content":
            {
                "raw": raw_string.strip("\"")
            },
        "inline":
            {
                "path": issue["filename"],
                "to": issue["line_number"]
            }
    }
    return content


def __get_url(bb_workspace: str,
              bb_repo_slug: str,
              bb_pr_id: str) -> str:
    return f"https://api.bitbucket.org/2.0/repositories/{bb_workspace}/{bb_repo_slug}/pullrequests/{bb_pr_id}/comments"


def __create_pr_comment(request_body: dict,
                        bb_workspace: str,
                        bb_repo_slug: str,
                        bb_pr_id: str,
                        username: str,
                        password: str):
    url = __get_url(bb_workspace, bb_repo_slug, bb_pr_id)
    r = requests.post(url=url, json=request_body, auth=(username, password))

    if r.status_code != 201:
        print(f"HTTP status code: {r.status_code}, reason: {r.reason}!", file=sys.stderr)
        raise Exception("Failed to create PR comment.")
    else:
        print(f"Created Bandit PR comment with following content: {request_body}!")


def main():
    __check_env_vars(ENV_VARS)

    scan_path = os.environ["SCAN_PATH"]
    bb_workspace = os.environ["BITBUCKET_WORKSPACE"]
    bb_repo_slug = os.environ["BITBUCKET_REPO_SLUG"]
    bb_pr_id = os.environ["BITBUCKET_PR_ID"]
    username = os.environ["BITBUCKET_USER"]
    password = os.environ["BITBUCKET_PWD"]
    bandit_output = __bandit_report(scan_path=scan_path, prepend_path=os.environ['BITBUCKET_CLONE_DIR'])
    [__create_pr_comment(request_body=__get_request_body(x),
                         bb_workspace=bb_workspace,
                         bb_repo_slug=bb_repo_slug,
                         bb_pr_id=bb_pr_id,
                         username=username,
                         password=password) for x in bandit_output["results"]]


if __name__ == "__main__":
    main()
