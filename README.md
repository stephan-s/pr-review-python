# Bitbucket piprot Pull Request Review
This Docker container helps you create automatic pull request comments.  
Provide the following variables in the pipeline, using account variables or repository variables.
Except for the `BITBUCKET_USER`, `BITBUCKET_PWD` and `REQUIREMENTS_FILE` everything should be provided automatically.

```
REQUIREMENTS_FILE - path to the requirements.txt
BITBUCKET_USER - Username of Bitbucket (bot) user
BITBUCKET_PWD - Password of Bitbucket (bot) user
BITBUCKET_PR_ID - Pull Request ID
BITBUCKET_REPO_SLUG - Slug of the repository
BITBUCKET_WORKSPACE - Workspace name

```

## Local testing
```shell
docker run \
-e "REQUIREMENTS_FILE=requirements.txt" \
-e "BITBUCKET_PR_ID=1" \
-e "BITBUCKET_REPO_SLUG=repo_name" \
-e "BITBUCKET_WORKSPACE=workspace_name" \
-e "BITBUCKET_USER=user@mail.com" \
-e "BITBUCKET_PWD=password" \
sschrijver/pr-review-python:latest
```

## Adding to bitbucket-pipelines.yml
```yaml
...
pipelines:
  pull-requests:
    '**':
      - step:
          name: PR Review
          script:
            - pipe: docker://sschrijver/pr-review-python:latest
              variables:
                REQUIREMENTS_FILE: requirements.txt
                BITBUCKET_USER: $BITBUCKET_BOT_USERNAME
                BITBUCKET_PWD: $BITBUCKET_BOT_PASSWORD
...
```