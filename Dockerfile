FROM python:3.9.6

USER root

RUN pip install piprot==0.9.11 requests==2.26.0 bandit==1.7.0

COPY pipe /

USER 1001

ENTRYPOINT ["python3", "/all_scans.py"]